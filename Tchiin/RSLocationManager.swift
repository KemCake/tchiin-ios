//
//  RSLocationManager.swift
//  Tchiin
//
//  Created by Remi Santos on 09/11/14.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

import UIKit

class RSLocationManager: CLLocationManager {
    
    // MARK: - Singleton
    class var sharedInstance: RSLocationManager {
        struct Static {
            static var instance: RSLocationManager?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = RSLocationManager()
        }
        
        return Static.instance!
    }
}
