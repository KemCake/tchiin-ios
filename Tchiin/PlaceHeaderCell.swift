//
//  PlaceHeaderCell.swift
//  Tchiin
//
//  Created by Remi Santos on 09/11/14.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

import UIKit

class PlaceHeaderCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var rateImageView: UIImageView!
    
    override init(){
        super.init()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    var place:Place! {
        didSet {
            titleLabel.text = place.name
            subtitleLabel.text = place.joinedCategories

            mainImageView.setImageWithURL(NSURL(string:place.imageUrl), placeholderImage: UIImage(named:"default_photo"))
            rateImageView.setImageWithURL(NSURL(string:place.ratingUrl))
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
