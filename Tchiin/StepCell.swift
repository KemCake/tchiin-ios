//
//  StepCell.swift
//  Thumbify
//
//  Created by Remi Santos on 18/12/14.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

import UIKit

class StepCell: UICollectionViewCell {
    
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    var parallaxPosition:CGFloat = 0 {
        didSet {
            titleLabel.alpha = 1
            descLabel.alpha = 1
            let y = self.contentView.frame.size.width/1.5 * parallaxPosition
            titleLabel.transform = CGAffineTransformMakeTranslation(y, 0)
            descLabel.transform = CGAffineTransformMakeTranslation(y, 0)
        }
    }
    
    override func prepareForReuse() {
        //prevent blink on reuse
        titleLabel.alpha = 0
        descLabel.alpha = 0
    }
}
