//
//  TNTabBarController.h
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TNLoginViewController.h"
#import "TNSignUpViewController.h"

@interface TNTabBarController : UITabBarController<PFLogInViewControllerDelegate,PFSignUpViewControllerDelegate>
{
    TNSignUpViewController *signUpViewController;
    TNLoginViewController *logInViewController;
}
-(void)logout;
@end
