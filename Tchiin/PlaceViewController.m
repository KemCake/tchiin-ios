//
//  PlaceViewController.m
//  Tchiin
//
//  Created by Remi Santos on 26/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "PlaceViewController.h"
#import "TOWebViewController.h"
#import <MSAlertController/MSAlertController.h>

@interface PlaceViewController ()

@end

@implementation PlaceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.emptyDataSetSource = self;
    if (self.launchOptions) {
        NSString *yelpID = self.launchOptions[@"yelp_id"];
        if (yelpID) {
            [[PlaceStore sharedInstance] getPlaceById:yelpID withBlock:^(NSArray *result) {
                if (result.count > 0) {
                    self.place = result[0];
                    [self loadPlace];
                    [self.tableView reloadData];
                } else {
                    [self.navigationController popViewControllerAnimated:true];
                }
            }];
        }
    } else if (self.place) {
        [self loadPlace];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:false animated:false];
}

- (void)loadPlace {
    self.title = self.place.name;
    NSDictionary *dimensions = @{@"name": @"place",
                                 @"place": self.place.yelpID,
                                 @"user": [PFUser currentUser].objectId};
    [PFAnalytics trackEvent:@"view" dimensions:dimensions];
}


-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    else
    {
        static NSString * const identifier = @"pin_orange";
        MKAnnotationView* annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView)
        {
            annotationView.annotation = annotation;
        }
        else
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        }
        annotationView.image = [UIImage imageNamed:identifier];
        return annotationView;
    }
    return nil;
}


-(void)openInMap {
    [self performSegueWithIdentifier:@"mapSegue" sender:nil];
}

- (void)launchCall {
    // Alert style
    MSAlertController *alert = [MSAlertController alertControllerWithTitle:@"Tchiin" message:[NSString stringWithFormat:@"%@ %@",LOCALIZED(@"Call"),self.place.name] preferredStyle:MSAlertControllerStyleAlert];
    

    MSAlertAction *cancelAction = [MSAlertAction actionWithTitle:LOCALIZED(@"Cancel") style:MSAlertActionStyleDefault handler:nil];
    cancelAction.titleColor = [UIColor tchiinOrange];
    MSAlertAction *callAction = [MSAlertAction actionWithTitle:LOCALIZED(@"Call") style:MSAlertActionStyleCancel handler:^(MSAlertAction *action) {
        NSString *URLString = [@"tel://" stringByAppendingString:self.place.phone];
        
        NSURL *URL = [NSURL URLWithString:URLString];
        
        [[UIApplication sharedApplication] openURL:URL];
    }];
    callAction.titleColor = [UIColor tchiinOrange];

    [alert addAction:cancelAction];
    [alert addAction:callAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)openInYelp {
    
    TOWebViewController *webView = [[TOWebViewController alloc] initWithURLString:self.place.yelpURL];
    [self.navigationController pushViewController:webView animated:true];
}


-(void)addAnnotationToMapView:(MKMapView*)mapView {
    [self.place getCoordinatesWithBlock:^(CLLocation *location) {
        if (location.coordinate.latitude != 0 || location.coordinate.longitude != 0) {
            TNPointAnnotation *point = [[TNPointAnnotation alloc] init];
            point.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
            point.title = self.place.name;
            point.subtitle = self.place.address;
            point.place = self.place;
            CLLocationDistance distance = [location distanceFromLocation:[RSLocationManager sharedInstance].location];
            point.subtitle = [[MKDistanceFormatter new] stringFromDistance:distance];
            [mapView addAnnotation:point];
            [mapView zoomToFitMapAnnotationsAnimated:true];
        }
    }];
}
#pragma mark - UITableView
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *ci = [self cellIdentifierForIndexPath:indexPath];
    if ([ci isEqualToString:@"headerCell"] || [ci isEqualToString:@"mapCell"]) {
        return 120;
    } else if ([ci isEqualToString:@"yelpCell"]) {
        if (self.place.bestReview[@"excerpt"] == nil) {
            return 0;
        }
        return 80;
    } else if ([ci isEqualToString:@"basicCell"]) {
        return 44;
    }
    return 44;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (!self.place) {
        return 0;
    }
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 3;
            break;
        case 2:
            return 2;
            break;
        default:
            return 1;
            break;
    }
}
-(NSString*)cellIdentifierForIndexPath:(NSIndexPath*)indexPath {
    if (indexPath.section == 0) {
        return @"headerCell";
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            return @"mapCell";
        }
        return @"basicCell";
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            return @"yelpCell";
        }
        return @"basicCell";
    }
    
    return @"basicCell";
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[self cellIdentifierForIndexPath:indexPath] forIndexPath:indexPath];
    if (indexPath.section == 0) {
        PlaceHeaderCell *cell = (PlaceHeaderCell*)[tableView dequeueReusableCellWithIdentifier:[self cellIdentifierForIndexPath:indexPath] forIndexPath:indexPath];
        cell.place = self.place;
        
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            MKMapView *mapView = (MKMapView*)[cell.contentView viewWithTag:1];
            mapView.delegate = self;
            [self addAnnotationToMapView:mapView];
        } else if (indexPath.row == 1) {
            cell.detailTextLabel.text = self.place.address;
            cell.textLabel.text = @"-";
            [self.place getCoordinatesWithBlock:^(CLLocation *location) {
                CLLocationDistance distance = [location distanceFromLocation:[RSLocationManager sharedInstance].location];
                cell.textLabel.text = [[MKDistanceFormatter new] stringFromDistance:distance];
                [cell setNeedsLayout];
            }];
        } else if (indexPath.row == 2) {
            cell.textLabel.text = LOCALIZED(@"CallThem");
            cell.detailTextLabel.text = self.place.displayPhone;
        }
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            if (self.place.bestReview[@"excerpt"] != nil) {
                cell.detailTextLabel.text = [NSString stringWithFormat:@"\"%@\"",self.place.bestReview[@"excerpt"]];
                cell.textLabel.text = self.place.bestReview[@"user"][@"name"];
            } else {
                cell.detailTextLabel.text = @"";
                cell.textLabel.text = @"";
            }
        } else {
            cell.textLabel.text = LOCALIZED(@"OpenInYelp");
            cell.detailTextLabel.text = @"";
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"yelp_logo"]];
            cell.backgroundColor = [UIColor yelpColor];
            cell.textLabel.textColor = [UIColor whiteColor];
        }
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        //nothing
    } else if (indexPath.section == 1) {
        if (indexPath.row == 2) {
            [self launchCall];
        } else {
            [self openInMap];
        }
    } else if (indexPath.section == 2) {
        [self openInYelp];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"mapSegue"])
    {
        PlaceMapViewController *destination = segue.destinationViewController;
        destination.place = self.place;
    }
}

#pragma mark - Empty data
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSString *text = LOCALIZED(@"LoadingText");
    if (failedToLoad) {
        text = LOCALIZED(@"Arh");
    }
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSString *text = LOCALIZED(@"WatchTheSky");
    if (failedToLoad) {
        text = LOCALIZED(@"AnErrorOccuredSubtitle");
    }
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
@end
