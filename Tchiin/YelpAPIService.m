//
//  YelpAPIService.m
//  YelpNearby
//
//  Created by Behera, Subhransu on 8/14/13.
//  Copyright (c) 2013 Behera, Subhransu. All rights reserved.
//

#import "YelpAPIService.h"
#import "OAuthAPIConstants.h"
#import "Place.h"

#define SEARCH_RESULT_LIMIT 30
#define SEARCH_FILTER @"bars"
#define SEARCH_SORT_POPULAR @"0"
#define SEARCH_DEFAULT_LOCATION @"France"

@implementation YelpAPIService
{
    NSURLConnection *connection;
}
-(void)searchNearByPlacesByTerm:(NSString *)term {
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@?term=%@&sort=%@&category_filter=%@&location=%@",
                           YELP_SEARCH_URL,
                           term,
                           SEARCH_SORT_POPULAR,
                           SEARCH_FILTER,
                           SEARCH_DEFAULT_LOCATION];
    [self searchNearByPlacesWithUrlString:urlString];
}
-(void)searchNearByPlacesByTerm:(NSString *)term atLatitude:(CLLocationDegrees)latitude
                          andLongitude:(CLLocationDegrees)longitude {
    
    NSString *urlString = [NSString stringWithFormat:@"%@?term=%@&sort=%@&category_filter=%@&ll=%f,%f",
                           YELP_SEARCH_URL,
                           term,
                           SEARCH_SORT_POPULAR,
                           SEARCH_FILTER,
                           latitude, longitude];
    [self searchNearByPlacesWithUrlString:urlString];
}
-(void)searchNearByPlacesByTerm:(NSString *)term betweenNorthEast:(CLLocationCoordinate2D)ne
                     andSouthWest:(CLLocationCoordinate2D)sw {
    
    NSString *urlString = [NSString stringWithFormat:@"%@?term=%@&sort=%@&category_filter=%@&bounds=%f,%f%%7C%f,%f",
                           YELP_SEARCH_URL,
                           term,
                           SEARCH_SORT_POPULAR,
                           SEARCH_FILTER,
                           (float)sw.latitude,(float)sw.longitude,
                           (float)ne.latitude,(float)ne.longitude];
    [self searchNearByPlacesWithUrlString:urlString];
}
-(void)searchNearByPlacesAtLatitude:(CLLocationDegrees)latitude
                       andLongitude:(CLLocationDegrees)longitude
                   withRadiusFilter:(NSInteger)radiusFilter {
    
    NSString *urlString = [NSString stringWithFormat:@"%@?term=%@&sort=%@&category_filter=%@&radius_filter=%ld&ll=%f,%f",
                           YELP_SEARCH_URL,
                           @"",
                           SEARCH_SORT_POPULAR,
                           SEARCH_FILTER,
                           (long)radiusFilter,
                           latitude, longitude];
    [self searchNearByPlacesWithUrlString:urlString];
}

-(void)getPlaceById:(NSString*)yelpId {
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@",
                           YELP_BUSINESS_URL,
                           yelpId];
    [self searchNearByPlacesWithUrlString:urlString];
}

-(void)searchNearByPlacesWithUrlString:(NSString*)urlString
{
    NSURL *URL = [NSURL URLWithString:urlString];
    
    OAConsumer *consumer = [[OAConsumer alloc] initWithKey:OAUTH_CONSUMER_KEY
                                                    secret:OAUTH_CONSUMER_SECRET];
    
    OAToken *token = [[OAToken alloc] initWithKey:OAUTH_TOKEN
                                           secret:OAUTH_TOKEN_SECRET];
    
    id<OASignatureProviding, NSObject> provider = [[OAHMAC_SHA1SignatureProvider alloc] init];
    NSString *realm = nil;
    
    OAMutableURLRequest *request = [[OAMutableURLRequest alloc] initWithURL:URL
                                                                   consumer:consumer
                                                                      token:token
                                                                      realm:realm
                                                          signatureProvider:provider];
    
    [request prepare];
    
    if (connection) {
        [connection cancel];
        connection = nil;
    }
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (connection) {
        self.urlRespondData = [NSMutableData data];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [self.urlRespondData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)d {
    [self.urlRespondData appendData:d];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {    
    NSLog(@"YelpService failed with error: %@",[error description]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError *e = nil;
    
    NSDictionary *resultResponseDict = [NSJSONSerialization JSONObjectWithData:self.urlRespondData
                                                                       options:NSJSONReadingMutableContainers
                                                                         error:&e];
    if (self.resultArray && [self.resultArray count] > 0) {
        [self.resultArray removeAllObjects];
    }
    
    if (!self.resultArray) {
        self.resultArray = [[NSMutableArray alloc] init];
    }
    
    if (resultResponseDict && [resultResponseDict count] > 0) {        
        if ([resultResponseDict objectForKey:@"businesses"] &&
            [[resultResponseDict objectForKey:@"businesses"] count] > 0) {
            for (NSDictionary *dic in [resultResponseDict objectForKey:@"businesses"]) {
                Place *placeObj = [[Place alloc] initWithDictionnary:dic];
                
                [self.resultArray addObject:placeObj];
            }
        } else if (![resultResponseDict objectForKey:@"error"] && ![resultResponseDict objectForKey:@"businesses"]){
            Place *placeObj = [[Place alloc] initWithDictionnary:resultResponseDict];
            
            [self.resultArray addObject:placeObj];
        }
    }
    
    [self.delegate loadResultWithDataArray:self.resultArray];
}



@end
