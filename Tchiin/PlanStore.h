//
//  PlanStore.h
//  Tchiin
//
//  Created by Remi Santos on 05/10/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PlanStore : NSObject

+(void)getLastPlanWithBlock:(void (^)(PFObject* object))block;
+(void)getCurrentPlansWithBlock:(void (^)(NSArray *objects))block;

@end
