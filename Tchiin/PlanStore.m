//
//  PlanStore.m
//  Tchiin
//
//  Created by Remi Santos on 05/10/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "PlanStore.h"

@implementation PlanStore

+(void)getLastPlanWithBlock:(void (^)(PFObject* object))block
{
    if ([PFUser currentUser]) {
        PFQuery *planDirect = [PFQuery queryWithClassName:@"Plan"];
        [planDirect whereKey:@"users" containsAllObjectsInArray:@[[PFUser currentUser]]];
        PFQuery *planPublic = [PFQuery queryWithClassName:@"Plan"];
        [planPublic whereKey:@"public" equalTo:[NSNumber numberWithBool:true]];
        
        PFQuery *mainQuery = [PFQuery orQueryWithSubqueries:@[planDirect, planPublic]];
        [mainQuery orderByDescending:@"start_date"];
        [mainQuery whereKey:@"end_date" greaterThan:[NSDate date]];
        [mainQuery whereKey:@"start_date" lessThan:[NSDate date]];
        [mainQuery includeKey:@"subscription"];
        [mainQuery includeKey:@"users"];
        [mainQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            
            block(object);
            
        }];
    }
}

+(void)getCurrentPlansWithBlock:(void (^)(NSArray *objects))block
{
    if ([PFUser currentUser]) {
        PFQuery *subs = [PFQuery queryWithClassName:@"Subscription"];
        [subs whereKey:@"users" containsAllObjectsInArray:@[[PFUser currentUser]]];
        PFQuery *plansSubs = [PFQuery queryWithClassName:@"Plan"];
        [plansSubs whereKey:@"subscription" matchesQuery:subs];
        
        PFQuery *plansDirect = [PFQuery queryWithClassName:@"Plan"];
        [plansDirect whereKey:@"users" containsAllObjectsInArray:@[[PFUser currentUser]]];
        
        PFQuery *plansPublic = [PFQuery queryWithClassName:@"Plan"];
        [plansPublic whereKey:@"public" equalTo:[NSNumber numberWithBool:true]];
        
        PFQuery *mainQuery = [PFQuery orQueryWithSubqueries:@[plansSubs,plansDirect, plansPublic]];
        [mainQuery orderByDescending:@"start_date"];
        [mainQuery whereKey:@"end_date" greaterThan:[NSDate date]];
        [mainQuery whereKey:@"start_date" lessThan:[NSDate date]];
        [mainQuery includeKey:@"subscription"];
        [mainQuery includeKey:@"users"];
        [mainQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            
            block(objects);
            
        }];
    }
}

@end
