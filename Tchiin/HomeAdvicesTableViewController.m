//
//  HomeAdvicesTableViewController.m
//  Tchiin
//
//  Created by Remi Santos on 20/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "HomeAdvicesTableViewController.h"

@interface HomeAdvicesTableViewController ()

@end

@implementation HomeAdvicesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)advicesUpdated
{
    [self.tableView reloadData];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.parent.advices.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AdviceCell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    PFObject *advice = self.parent.advices[indexPath.row];
    UIView *subContentView = [cell.contentView viewWithTag:4];
    subContentView.layer.cornerRadius = 5;
    subContentView.clipsToBounds = YES;
    UILabel *nameLabel = (UILabel*)[cell.contentView viewWithTag:1];
    nameLabel.text = advice[@"title"];
    
    UILabel *descLabel = (UILabel*)[cell.contentView viewWithTag:2];
    descLabel.text = advice[@"description"];
    
    UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag:3];
    [advice[@"image"] getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
        if (!error) {
            UIImage *image = [UIImage imageWithData:data];
            imageView.image = image;
        }
    }];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PFObject *advice = self.parent.advices[indexPath.row];
    if([advice[@"type"] isEqualToString:@"place"]){
        //launch place
        [self performSegueWithIdentifier:@"PlaceSegue" sender:self];
    } else if([advice[@"type"] isEqualToString:@"map"]){
        //launch map
        [self performSegueWithIdentifier:@"MapSegue" sender:self];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    PFObject *advice = self.parent.advices[indexPath.row];
    if ([segue.identifier isEqualToString:@"PlaceSegue"]){
        PlaceViewController *destination = [segue destinationViewController];
        destination.launchOptions = advice[@"params"];
    } else if ([segue.identifier isEqualToString:@"MapSegue"]){
        //MapListViewController *destination = [segue destinationViewController];
        //destination.launchOptions = self.adviceParams;
    }
}
@end
