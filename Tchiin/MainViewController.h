//
//  MainViewController.h
//  Tchiin
//
//  Created by Remi Santos on 17/09/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TNTabBarController.h"
#import "PlanStore.h"
#import "PlaceViewController.h"
#import "AppDelegate.h"
#import "UIViewController+MaryPopin.h"
#import "MapListViewController.h"
#import "Tchiin-Swift.h"

@interface MainViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,PlanCollectionViewCellDelegate, PopupViewControllerDelegate>
{
    NSArray *currentPlans;
    BOOL isShowingIntro;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) MapListViewController *mapViewController;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *infoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoSubtitleLabel;
- (IBAction)showMapButtonClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *reloadButton;
- (IBAction)reloadButtonClicked:(id)sender;


@end
