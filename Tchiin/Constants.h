//
//  Constants.h
//  Tchiin
//
//  Created by Remi Santos on 21/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#define LOCALIZED(key) NSLocalizedString(key,@"")



#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define kUDFirstLaunch @"UDFirstLaunch"

#define IS_IPHONE_6 [[UIScreen mainScreen] bounds].size.height >= 667

#define GOOGLE_API_KEY @"AIzaSyA2AI1IszuTkA8lKt6n9Rwr_ryhWsxp6gU"