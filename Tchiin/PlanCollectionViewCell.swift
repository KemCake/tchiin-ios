//
//  PlanCollectionViewCell.swift
//  Tchiin
//
//  Created by Remi Santos on 14/10/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

import UIKit

@objc protocol PlanCollectionViewCellDelegate:NSObjectProtocol {
    optional func planSelectedAtIndex(index:NSIndexPath)
}

class PlanCollectionViewCell: UICollectionViewCell {
    
    var indexPath:NSIndexPath?
    var delegate:PlanCollectionViewCellDelegate?
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var subscriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var plan:PFObject = PFObject(className: "Plan") {
        didSet {
            titleLabel.text = plan["title"] as? String
            subtitleLabel.text = plan["subtitle"] as? String
            
            var byTitle = "Tchiin"
            if let subscription = plan["subscription"] as? PFObject {
                byTitle = subscription["title"] as String
            }
            
            if let buttonTitle = plan["button"] as? String {
                actionButton .setTitle(buttonTitle, forState: UIControlState.Normal)
            }
            var attributes = NSMutableAttributedString(string: NSLocalizedString("OfferedBy",comment:""))
            let boldAttr = NSAttributedString(string: byTitle, attributes: [NSFontAttributeName:UIFont.boldSystemFontOfSize(12)])
            attributes.appendAttributedString(boldAttr)
            subscriptionLabel.attributedText = attributes
            
            if let file = plan["image"] as? PFFile {
                imageView.setImageWithURLRequest(NSURLRequest(URL: NSURL(string: file.url)!), placeholderImage: imageView.image, success: { (request, response, image) -> Void in
                    self.imageView.alpha = 1
                    self.imageView.image = image
                    self.imageView.contentMode = UIViewContentMode.ScaleAspectFill
                }, failure: nil)
            }
            else if let file = plan["icon"] as? PFFile {
                imageView.setImageWithURLRequest(NSURLRequest(URL: NSURL(string: file.url)!), placeholderImage: imageView.image, success: { (request, response, image) -> Void in
                    self.imageView.alpha = 1
                    self.imageView.image = image
                    self.imageView.contentMode = UIViewContentMode.Center
                    }, failure: nil)
            }
        }
    }
    
    override func awakeFromNib() {
        self.containerView.layer.borderColor = UIColor.grayColor().colorWithAlphaComponent(0.5).CGColor
        self.containerView.layer.borderWidth = 1.0
        
    }
    @IBAction func actionButtonClicked(sender: UIButton) {

            self.delegate?.planSelectedAtIndex?(self.indexPath!)
    }
}
