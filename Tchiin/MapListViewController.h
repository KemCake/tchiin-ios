//
//  MapListViewController.h
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "QMBParallaxScrollViewController.h"
#import "MapViewController.h"
#import "ListViewController.h"
#import "AFNetworking.h"
#import "RSBackView.h"

@class MapViewController;
@class ListViewController;
@interface MapListViewController : QMBParallaxScrollViewController<QMBParallaxScrollViewControllerDelegate,UISearchBarDelegate>
{
    BOOL isFullTopView;
}

@property (nonatomic, strong) NSDictionary *launchOptions;

@property (nonatomic, strong) NSMutableArray *places;
@property (nonatomic) MapViewController *mapVC;
@property (nonatomic) ListViewController *listVC;
@property (nonatomic) MKDistanceFormatter *distanceFormatter;

-(void)loadPlacesForLocation:(CLLocation*)location;
-(void)loadPlacesForNorthEast:(CLLocation*)ne andSouthWest:(CLLocation*)sw;


@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;

@property (nonatomic) UISearchBar *searchBar;


@property (nonatomic, strong) NSMutableArray* searchPlaces;
@property (nonatomic) BOOL isSearching;

@end
