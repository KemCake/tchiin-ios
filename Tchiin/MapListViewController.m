//
//  MapListViewController.m
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "MapListViewController.h"

@implementation MapListViewController

static NSString *placeCellIdentifier = @"PlaceCell";

-(void)viewDidLoad
{
    [super viewDidLoad];

    self.mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapVC"];
    [self.mapVC setShowToolbar:true];
    self.mapVC.parent = self;
    self.listVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ListVC"];
    self.listVC.parent = self;
    [self setupWithTopViewController:self.mapVC andTopHeight:(self.view.frame.size.height/4)*3 andBottomViewController:self.listVC];
    self.maxHeight = self.view.frame.size.height-60.0;
    [self enableTapGestureTopView:NO];
    self.delegate = self;

    self.distanceFormatter = [[MKDistanceFormatter alloc] init];

    //Launch Options
    if (self.launchOptions) {
        
    }
    else {
        self.mapVC.loadOnLocation = TRUE;
    }
    
    //Search Controller
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.frame.size.width - 50, 44.0)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = NSLocalizedString(@"FindRightPlace",@"");
    self.searchBar.userInteractionEnabled=YES;
    UIView *searchBarContainer = [[UIView alloc] initWithFrame:self.searchBar.frame];
    [searchBarContainer addSubview:self.searchBar];
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    UIBarButtonItem *searchBarItem = [[UIBarButtonItem alloc] initWithCustomView:searchBarContainer];
    self.navigationItem.rightBarButtonItem = searchBarItem;
    
    UITextField *searchField = [self.searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor whiteColor];
    [searchField setValue:[UIColor colorWithWhite:1 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    UIImageView *leftView = (id)searchField.leftView;
    leftView.image = [leftView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    leftView.tintColor = [UIColor whiteColor];

 }

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSDictionary *dimensions = @{@"name": @"map",
                                 @"user": [PFUser currentUser].objectId};
    [PFAnalytics trackEvent:@"view" dimensions:dimensions];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:false animated:true];
}



-(void)showLoadingIndicator {
    [self.mapVC.activityIndicator startAnimating];
}
-(void)hideLoadingIndicator {
    [self.mapVC.activityIndicator stopAnimating];
}

-(void)loadPlacesForLocation:(CLLocation*)location
{
    [self showLoadingIndicator];
    self.isSearching = false;
    [[PlaceStore sharedInstance] searchPlacesAroundLocation:location withRadiusFilter:1000 withBlock:^(NSArray *places) {
        self.places = [places mutableCopy];
        [self.mapVC placesUpdated];
        [self.listVC placesUpdated];
        [self hideLoadingIndicator];
        NSDictionary *dimensions = @{@"name": @"place",
                                     @"count": [NSString stringWithFormat:@"%lu",(unsigned long)places.count],
                                     @"user": [PFUser currentUser].objectId};
        [PFAnalytics trackEvent:@"load" dimensions:dimensions];
    }];
    
}
-(void)loadPlacesForNorthEast:(CLLocation*)ne andSouthWest:(CLLocation*)sw
{
    [self showLoadingIndicator];
    self.isSearching = false;
    [[PlaceStore sharedInstance] getAllPlaceBetweenNorthEast:ne andSouthWest:sw withBlock:^(NSArray *places) {
        self.places = [places mutableCopy];
        [self.mapVC placesUpdated];
        [self.mapVC.mapView fitBoundsWithNorthEast:ne andSouthWest:sw];
        [self.listVC placesUpdated];
        [self hideLoadingIndicator];
        NSDictionary *dimensions = @{@"name": @"place",
                                     @"count": [NSString stringWithFormat:@"%lu",(unsigned long)places.count],
                                     @"user": [PFUser currentUser].objectId};
        [PFAnalytics trackEvent:@"load" dimensions:dimensions];
    }];
    
}

-(void)showMap
{
    [self showFullTopView:YES];
}
-(void)showList
{
    [self showFullTopView:NO];
}
#pragma mark - QMBParallax
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [super scrollViewDidScroll:scrollView];
}
-(void)parallaxScrollViewController:(QMBParallaxScrollViewController *)controller didChangeState:(QMBParallaxState)state
{
    if (state == QMBParallaxStateFullSize) {
        self.mapVC.showToolbar = TRUE;
    }
    else {
        self.mapVC.showToolbar = FALSE;
    }
}

#pragma mark - Search Bar
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:YES animated:YES];
    self.isSearching = true;
    [self.parallaxScrollView setContentOffset:CGPointMake(0, 200) animated:true];
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    [self.parallaxScrollView setContentOffset:CGPointZero animated:true];
    return YES;
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar endEditing:true];
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
    self.isSearching = false;
    searchBar.text = @"";
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText isEqualToString:@""]) {
        [self hideLoadingIndicator];
        return;
    }
    [self showLoadingIndicator];
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:self.mapVC.mapView.centerCoordinate.latitude longitude:self.mapVC.mapView.centerCoordinate.longitude];
    [[PlaceStore sharedInstance] searchPlacesAroundLocation:centerLocation withTerm:searchBar.text withBlock:^(NSArray *places) {
        self.searchPlaces = [places mutableCopy];
        [self.mapVC placesUpdated];
        [self.listVC placesUpdated];
        [self hideLoadingIndicator];
        NSDictionary *dimensions = @{@"name": @"search",
                                     @"count": [NSString stringWithFormat:@"%lu",(unsigned long)places.count],
                                     @"user": [PFUser currentUser].objectId};
        [PFAnalytics trackEvent:@"load" dimensions:dimensions];
    }];
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar endEditing:true];
}

@end
