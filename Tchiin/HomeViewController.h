//
//  FirstViewController.h
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QMBParallaxScrollViewController.h"
#import "HomeMainLinksCollectionViewController.h"
#import "HomeAdvicesTableViewController.h"
#import "MainLinkStore.h"
#import "AdviceStore.h"
#import "AFNetworking.h"
#import "MapListViewController.h"
@class HomeMainLinksCollectionViewController;
@class HomeAdvicesTableViewController;

@interface HomeViewController : QMBParallaxScrollViewController<UITableViewDataSource,UITableViewDelegate,UISearchControllerDelegate, UISearchDisplayDelegate>
@property (nonatomic, strong) HomeMainLinksCollectionViewController* mainLinksVC;
@property (nonatomic, strong) HomeAdvicesTableViewController* advicesVC;

@property (nonatomic, strong) NSMutableArray* mainLinks;
@property (nonatomic, strong) NSMutableArray* advices;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


@property (nonatomic, strong) NSMutableArray* searchAutocomplete;

@end
