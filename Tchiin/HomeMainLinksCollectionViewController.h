//
//  HomeMainLinksCollectionViewController.h
//  Tchiin
//
//  Created by Remi Santos on 20/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
@class HomeViewController;
@interface HomeMainLinksCollectionViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>

@property (nonatomic, strong) HomeViewController* parent;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

-(void)mainLinksUpdated;
@end
