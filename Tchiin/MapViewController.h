//
//  MapViewController.h
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "PlaceStore.h"
#import "MapListViewController.h"
#import "MKMapView+Utils.h"
#import "MKMapView+ZoomLevel.h"

@class MapListViewController;
@interface MapViewController : UIViewController<CLLocationManagerDelegate,MKMapViewDelegate>
{

}
@property (nonatomic, strong) MapListViewController* parent;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *reloadButton;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (nonatomic, strong) UIActivityIndicatorView* activityIndicator;

@property (nonatomic) BOOL showToolbar;
@property (nonatomic) BOOL loadOnLocation;

- (IBAction)reloadRegion:(id)sender;
-(void)placesUpdated;
@end
