//
//  FirstViewController.m
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mainLinksVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MainLinksVC"];
    self.mainLinksVC.parent = self;
    self.advicesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AdvicesVC"];
    self.advicesVC.parent = self;
    [self setupWithTopViewController:self.mainLinksVC andTopHeight:250 andBottomViewController:self.advicesVC];
    self.maxHeight = 250;
    [self enableTapGestureTopView:NO];

    [MainLinkStore getAllMainLinksWithBlock:^(NSMutableArray *mainLinks) {
        self.mainLinks = mainLinks;
        [self.mainLinksVC mainLinksUpdated];
    }];
    [AdviceStore getAllAdvicesWithBlock:^(NSMutableArray *advices) {
        self.advices = advices;
        [self.advicesVC advicesUpdated];
    }];
    
    //Search Controller
    self.searchDisplayController.displaysSearchBarInNavigationBar = YES;
    self.searchDisplayController.searchBar.placeholder = @"Trouves le bon endroit";
    self.navigationItem.title = @"Suggestions";

    UITextField *searchField = [self.searchDisplayController.searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor whiteColor];
    [searchField setValue:[UIColor colorWithWhite:1 alpha:0.8] forKeyPath:@"_placeholderLabel.textColor"];
    UIImageView *leftView = (id)searchField.leftView;
    leftView.image = [leftView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    leftView.tintColor = [UIColor whiteColor];
    
    CGRect frame = self.searchDisplayController.searchResultsTableView.frame;
    frame.size.height = 220;
    [self.searchDisplayController.searchResultsTableView setFrame:frame];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Search Controller

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    if(searchString.length > 0)
    {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSDictionary *googleParameters = @{@"key": GOOGLE_API_KEY,
                                           @"input": searchString
                                           };
        [manager GET:@"https://maps.googleapis.com/maps/api/place/autocomplete/json" parameters:googleParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            self.searchAutocomplete = [NSMutableArray arrayWithArray:responseObject[@"predictions"]];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }
    return YES;
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:YES animated:YES];
    
    return YES;
}
-(void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    UISearchBar *searchBar = controller.searchBar;
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchAutocomplete.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    const NSString *identifier = @"SearchCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    if(self.searchAutocomplete.count > 0)
        cell.textLabel.text = self.searchAutocomplete[indexPath.row][@"description"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MapListViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"MapListVC"];
    NSLog(@"index row : %d", indexPath.row);
    NSLog(@"place : %@",self.searchAutocomplete[indexPath.row]);
    destination.launchOptions = @{@"google_place":self.searchAutocomplete[indexPath.row]};
    [self.navigationController pushViewController:destination animated:YES];
}
@end
