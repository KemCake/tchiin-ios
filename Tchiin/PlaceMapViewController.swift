//
//  PlaceMapViewController.swift
//  Tchiin
//
//  Created by Remi Santos on 20/10/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

import UIKit
import MapKit

class PlaceMapViewController: UIViewController, MKMapViewDelegate{

    @IBOutlet weak var mapView: MKMapView!
    var place:Place!
    var locationManager = CLLocationManager()
    var location = CLLocation()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mapView.showsUserLocation = true;
        self.place.getCoordinatesWithBlock { (location) -> Void in
            self.location = location
            var point = TNPointAnnotation()
            point.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
            point.title = self.place.name
            point.subtitle = self.place.address
            point.place = self.place
            self.mapView.addAnnotation(point)
            self.mapView.zoomToFitWithAnnotations(self.mapView.annotationsWithoutUserLocation(), animated: true);
        }
        
        let userLocationButton = MKUserTrackingBarButtonItem(mapView: self.mapView)
        let actionButton = UIBarButtonItem(title: NSLocalizedString("Maps",comment:""), style: UIBarButtonItemStyle.Plain, target: self, action: "planButtonClick:")
        self.locationManager.requestWhenInUseAuthorization()
        self.navigationItem.rightBarButtonItems = [actionButton, userLocationButton]

        self.mapView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if (annotation.isKindOfClass(MKUserLocation)) {
            return nil
        }
        
        let identifier = "pin_orange"
        var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
        if (annotationView != nil) {
            annotationView.annotation = annotation;
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        }
        annotationView.canShowCallout = true;
        annotationView.annotation = annotation
        annotationView.image = UIImage(named: identifier)
        return annotationView
    }

    func planButtonClick(sender:AnyObject) {

        let placemark = MKPlacemark(coordinate: self.location.coordinate, addressDictionary: nil)
        let item = MKMapItem(placemark: placemark)
        item.name = self.place.name;
        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeWalking]

        item.openInMapsWithLaunchOptions(launchOptions)
    } 

}
