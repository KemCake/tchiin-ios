//
//  MainViewController.m
//  Tchiin
//
//  Created by Remi Santos on 17/09/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:true animated:true];
    self.collectionView.backgroundColor = [UIColor tchiinBlueColor];
    UIBarButtonItem *newBackButton =
    [[UIBarButtonItem alloc] initWithTitle:@""
                                     style:UIBarButtonItemStyleBordered
                                    target:nil
                                    action:nil];
    [self.navigationItem setBackBarButtonItem:newBackButton];
    
    self.infoView.hidden = true;
    self.infoView.backgroundColor = [UIColor clearColor];
    self.collectionView.delegate = self;
    
    self.reloadButton.hidden = true;

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (![userDefaults boolForKey:kUDFirstLaunch]) {
        [userDefaults setBool:true forKey:kUDFirstLaunch];
        [self showIntro];
    } else {
        [self refresh];
    }
    
    self.mapViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MapListVC"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"RefreshMainViewNotification" object:nil];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSDictionary *dimensions = @{@"name": @"home",
                                 @"user": [PFUser currentUser].objectId?:@""};
    [PFAnalytics trackEvent:@"view" dimensions:dimensions];
    
//    [self showIntroViewController];
}
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    if (![PFUser currentUser]) {
        [PFAnonymousUtils logInWithBlock:^(PFUser *user, NSError *error) {
            [self refresh];
            NSDictionary *dimensions = @{@"type": @"anonymous"};
            [PFAnalytics trackEvent:@"login" dimensions:dimensions];
        }];
    }
    [self.navigationController setNavigationBarHidden:true animated:true];
}
-(void)showIntroViewController
{
    [self performSegueWithIdentifier:@"IntroSegue" sender:nil];
}

-(void)refresh {
    if (!isShowingIntro) {
        [self.activityIndicator startAnimating];
        self.reloadButton.hidden = true;
        [PlanStore getCurrentPlansWithBlock:^(NSArray *objects) {
            
            currentPlans = objects;
            self.infoView.hidden = true;
            if (objects.count <= 0) {
                self.infoView.hidden = false;
                self.infoTitleLabel.text = NSLocalizedString(@"NoPlanTitle",@"");
                self.infoSubtitleLabel.text = NSLocalizedString(@"NoPlanSubtitle",@"");
            }
            [self.activityIndicator stopAnimating];
            self.reloadButton.hidden = false;
            [self.collectionView reloadData];
            
            NSDictionary *dimensions = @{@"name": @"plan",
                                         @"count": [NSString stringWithFormat:@"%lu",(unsigned long)objects.count],
                                         @"user": [PFUser currentUser].objectId};
            [PFAnalytics trackEvent:@"load" dimensions:dimensions];
        }];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView datasource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return currentPlans.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PlanCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PlanCell" forIndexPath:indexPath];
    
    cell.plan = currentPlans[indexPath.row];
    cell.delegate = self;
    cell.indexPath = indexPath;
    CGRect finalCellFrame = cell.frame;

    CGPoint translation = [collectionView.panGestureRecognizer translationInView:collectionView.superview];
    if (translation.x > 0) {
        cell.frame = CGRectMake(finalCellFrame.origin.x - 100, finalCellFrame.origin.y + 100.0f, 0, 0);
    } else {
        cell.frame = CGRectMake(finalCellFrame.origin.x + 100, finalCellFrame.origin.y + 100.0f, 0, 0);
    }
    
    [UIView animateWithDuration:0.8 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:0.5 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        cell.frame = finalCellFrame;
    } completion:nil];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        return CGSizeMake(self.collectionView.frame.size.height, self.collectionView.frame.size.height);
    }
    return self.collectionView.frame.size;
}


-(void)planSelectedAtIndex:(NSIndexPath *)indexPath {
    
    PFObject *plan = currentPlans[indexPath.row];
    if (plan[@"yelp_id"]) {
        PlaceViewController *placeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceVC"];
        placeVC.launchOptions = @{@"yelp_id":plan[@"yelp_id"]};
        [self.navigationController setNavigationBarHidden:false animated:true];
        [self.navigationController pushViewController:placeVC animated:true];
    }
}



#pragma mark - Intro
-(void)showIntro {
    isShowingIntro = true;
    
    UIViewController *popup1 = [self.storyboard instantiateViewControllerWithIdentifier:@"PopupFirst"];
    [popup1 setPopinOptions:BKTPopinDisableParallaxEffect];
    [popup1 setPopinOptions:BKTPopinDisableAutoDismiss|BKTPopinDimmingViewStyleNone];
    [popup1 setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
    [popup1 setPopinTransitionStyle:BKTPopinTransitionStyleSpringySlide];
    CGRect presentationRect = CGRectMake(0, 0, 300, 220);
    popup1.view.frame = presentationRect;
    popup1.view.center = self.view.center;
    popup1.view.layer.cornerRadius = 10;
    popup1.view.tag = 0;
    popup1.view.backgroundColor = [UIColor clearColor];
    [self presentPopinController:popup1 animated:YES completion:^{
        
    }];
    
}
-(void)showSecondIntro {
    UIViewController *popup2 = [self.storyboard instantiateViewControllerWithIdentifier:@"PopupSecond"];
    [popup2 setPopinOptions:BKTPopinDisableParallaxEffect];
    [popup2 setPopinTransitionDirection:BKTPopinTransitionDirectionTop];
    [popup2 setPopinTransitionStyle:BKTPopinTransitionStyleSpringySlide];
    [popup2 setPopinOptions:BKTPopinDisableAutoDismiss|BKTPopinDimmingViewStyleNone];
    CGRect presentationRect = CGRectMake(0, 0, 300, 280);
    popup2.view.frame = presentationRect;
    popup2.view.center = self.view.center;
    popup2.view.layer.cornerRadius = 10;
    popup2.view.tag = 1;
    popup2.view.backgroundColor = [UIColor clearColor];
    [self presentPopinController:popup2 animated:YES completion:^{
        
    }];
}
-(void)popupDidDismissWithIndex:(NSInteger)index {
    if (index == 0) {
        [self showSecondIntro];
    } else if (index == 1) {
        
        isShowingIntro = false;
        AppDelegate * delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [delegate registerForNotifications];
        [self refresh];
    }
}
- (IBAction)showMapButtonClicked:(id)sender {
    [self.navigationController pushViewController:self.mapViewController animated:true];
}

- (IBAction)reloadButtonClicked:(id)sender {
    [self refresh];
}
@end
