//
//  Restaurant.m
//  YelpNearby
//
//  Created by Behera, Subhransu on 8/14/13.
//  Copyright (c) 2013 Behera, Subhransu. All rights reserved.
//

#import "Place.h"

@implementation Place
- (instancetype)initWithDictionnary:(NSDictionary*)dic
{
    self = [super init];
    if (self) {
        _name = dic[@"name"];
        _address = [NSString stringWithFormat:@"%@, %@ %@", dic[@"location"][@"address"][0],dic[@"location"][@"postal_code"],dic[@"location"][@"city"]];
        _imageUrl = dic[@"image_url"]?:@"";
        _ratingUrl = dic[@"rating_img_url"]?:@"";
        _yelpURL = dic[@"url"]?:@"";
        
        _phone = dic[@"phone"]?:@"";
        _displayPhone = dic[@"display_phone"]?:@"";
        _yelpID = dic[@"id"];
        _categories = dic[@"categories"];
        _isClosed = (BOOL)dic[@"is_closed"];
        _bestReview = dic[@"reviews"][0]?:@{};
        _data = dic;
    }
    return self;
}

-(void)getCoordinatesWithBlock:(void (^)(CLLocation *location))block
{
    if (self.location) {
        block(self.location);
    } else {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:self.address completionHandler:^(NSArray* placemarks, NSError* error){
            CLPlacemark* aPlacemark = placemarks[0];
            block(aPlacemark.location);
        }];
    }
}

-(NSString *)joinedCategories {
    if (!_joinedCategories) {
        NSMutableArray *categories = [NSMutableArray array];
        for (NSArray *categorie in self.categories) {
            [categories addObject:categorie[0]];
        }
        _joinedCategories = [categories componentsJoinedByString:@", "];
    }
    return _joinedCategories;
}
@end
