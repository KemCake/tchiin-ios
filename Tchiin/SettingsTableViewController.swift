//
//  SettingTableViewController.swift
//  Tchiin
//
//  Created by Remi Santos on 13/10/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController, UIAlertViewDelegate {

    var settings = [NSLocalizedString("AboutTchiin", comment:"")]
    var subscriptions:[PFObject] = []
    var loadingSubs = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Tchiin"
        
//        let splitVC = self.splitViewController!
//        splitVC.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
//        splitVC.delegate = self
//        splitVC.preferredPrimaryColumnWidthFraction = 0.3
        
        refreshSubscriptions()
        let dimensions = ["name":"settings",
                          "user":PFUser.currentUser().objectId]
        PFAnalytics.trackEventInBackground("view", dimensions:dimensions, block:nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController!, ontoPrimaryViewController primaryViewController: UIViewController!) -> Bool {
//        if secondaryViewController.isKindOfClass(UINavigationController) {
//            return true
//        }
//        return true
//    }

    @IBAction func closeSettingsButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func refreshSubscriptions() {
        loadingSubs = true
        SubscriptionStore.getAllSubscriptionsForCurrentUserWithBlock { (results) -> Void in
            self.subscriptions = results
            self.loadingSubs = false
            self.tableView.reloadData()
            let dimensions = ["name":"subscription",
                              "count":String(results.count),
                "user":PFUser.currentUser().objectId]
            PFAnalytics.trackEventInBackground("load", dimensions:dimensions, block:nil)
        }
    }
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (section == 0) {
            return subscriptions.count + 1
        }
        return settings.count
    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0) {
            return NSLocalizedString("Subscriptions", comment:"")
        }
        return ""
    }
    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if(section == 1) {
            return "© Tchiin 2014 - "+PFUser.currentUser().objectId
        }
        return ""
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellIdentifier", forIndexPath: indexPath) as UITableViewCell

        if (indexPath.section == 0) {
            cell.textLabel?.textColor = UIColor.blackColor()
            if (indexPath.row >= subscriptions.count) {
                if (self.loadingSubs) {
                    cell.textLabel?.text = NSLocalizedString("LoadingSubscriptions", comment:"")
                    cell.textLabel?.textColor = UIColor.lightGrayColor()
                } else {
                    cell.textLabel?.text = NSLocalizedString("AddASubscription", comment:"")
                }
            } else {
                let subscription = subscriptions[indexPath.row]
                cell.textLabel?.text = subscription["title"] as? String
            }
        } else {
            cell.textLabel?.text = settings[indexPath.row]
        }


        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.tableView .deselectRowAtIndexPath(indexPath, animated: true)
        switch indexPath.section {
            case 1:
                if (indexPath.row == 0) {
                    
                    let webView = TOWebViewController(URLString: "http://tchi.in/")
                    self.navigationController?.pushViewController(webView, animated: true)
                }
            default:
                if (indexPath.row >= subscriptions.count && !self.loadingSubs) {
                    
                    self.showCodeAlert()
                    
                }
        }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if(indexPath.section == 0) {
            return true
        }
        return false
    }
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section == 0 && editingStyle == UITableViewCellEditingStyle.Delete) {
            let sub = subscriptions[indexPath.row]
            
            SubscriptionStore.unsubscribeWithId(sub.objectId, withBlock: { (succeed) -> Void in
                
            })
            
            subscriptions.removeAtIndex(indexPath.row)
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)
        }
    }
    
    // MARK : UIAlert
    var alertTextField:UITextField?
    func showCodeAlert() {
        var alertController = MSAlertController(title: NSLocalizedString("EnterCodeSubscription", comment:""), message: "", preferredStyle: .Alert)
        let cancelAction = MSAlertAction(title: NSLocalizedString("Cancel", comment:""), style: .Cancel, handler:nil)
        cancelAction.titleColor = UIColor.tchiinOrange()
        alertController.addAction(cancelAction)
        
        let addAction = MSAlertAction(title: NSLocalizedString("Add", comment:""), style: .Default, handler: { (alertAction) -> Void in
            let text = self.alertTextField?.text
            if (text != nil) {
                SubscriptionStore.subscribeWithCode(text!, withBlock: { (succeed) -> Void in
                    if (succeed) {
                        self.refreshSubscriptions()
                    }
                })
            }
        })
        addAction.titleColor = UIColor.tchiinOrange()
        alertController.addAction(addAction)
        
        alertController.addTextFieldWithConfigurationHandler({ (textField) -> Void in
            textField.placeholder = "Code..."
            self.alertTextField = textField
        })
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if(buttonIndex == alertView.cancelButtonIndex) {
            return
        }
        let text = alertView.textFieldAtIndex(0)?.text
        if (text != nil) {
            SubscriptionStore.subscribeWithCode(text!, withBlock: { (succeed) -> Void in
                if (succeed) {
                    self.refreshSubscriptions()
                }
            })
        }
    }
}
