//
//  UIColor+Custom.h
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Custom)
+(UIColor*)tchiinBrown;
+(UIColor*)tchiinOrange;
+(UIColor*)tchiinOrangeLight;
+(UIColor*)tchiinBlueColor;
+(UIColor*)tchiinComplementaryLightBlue;
+(UIColor*)tchiinComplementaryDarkBlue;

+(UIColor*)facebookColor;
+(UIColor*)yelpColor;
@end
