//
//  MainLinkStore.m
//  Tchiin
//
//  Created by Remi Santos on 20/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "MainLinkStore.h"

@implementation MainLinkStore

+(void)getAllMainLinksWithBlock:(void (^)(NSMutableArray* mainLinks))block
{
    PFQuery *query = [PFQuery queryWithClassName:@"MainLink"];
    [query orderByAscending:@"order"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *result = [NSMutableArray arrayWithArray:objects];
        block(result);
    }];
}
@end
