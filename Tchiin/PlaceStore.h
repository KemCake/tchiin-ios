//
//  PlaceStore.h
//  Tchiin
//
//  Created by Remi Santos on 19/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YelpAPIService.h"
#import "Place.h"
@interface PlaceStore : NSObject<YelpAPIServiceDelegate>
{
    void (^currentBlock)(NSArray* places);
    YelpAPIService *yelpService;
}
+ (id)sharedInstance;

-(void)searchPlacesWithTerm:(NSString*)term withBlock:(void (^)(NSArray* places))block;
-(void)searchPlacesAroundLocation:(CLLocation*)location withTerm:(NSString*)term withBlock:(void (^)(NSArray* places))block;
-(void)searchPlacesAroundLocation:(CLLocation*)location withBlock:(void (^)(NSArray* places))block;
-(void)getAllPlaceBetweenNorthEast:(CLLocation*)ne andSouthWest:(CLLocation*)sw withBlock:(void (^)(NSArray* places))block;
-(void)searchPlacesAroundLocation:(CLLocation*)location withRadiusFilter:(NSInteger)radiusFilter withBlock:(void (^)(NSArray* places))block;

-(void)getPlaceById:(NSString*)yelpId withBlock:(void (^)(NSArray* result))block;

@end
