//
//  Restaurant.h
//  YelpNearby
//
//  Created by Behera, Subhransu on 8/14/13.
//  Copyright (c) 2013 Behera, Subhransu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@interface Place : NSObject
- (instancetype)initWithDictionnary:(NSDictionary*)dic;

@property (nonatomic, strong) NSDictionary *data;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *ratingUrl;
@property (nonatomic, strong) NSString *yelpURL;

@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *displayPhone;

@property (nonatomic, strong) NSString *yelpID;
@property (nonatomic, strong) NSArray *categories;
@property (nonatomic) BOOL isClosed;
@property (nonatomic, strong) NSDictionary *bestReview;

@property (nonatomic, strong) CLLocation *location;

-(void)getCoordinatesWithBlock:(void (^)(CLLocation *location))block;

@property (nonatomic, strong) NSString *joinedCategories;

@end
