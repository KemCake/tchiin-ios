//
//  TNSignUpViewController.m
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "TNSignUpViewController.h"

@implementation TNSignUpViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self.signUpView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bar1"]]];
    [self.signUpView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.0]];

    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
    logo.frame = CGRectMake(0, 0, 200, 60);
    [self.signUpView setLogo:logo];
    
    self.signUpView.usernameField.backgroundColor = [UIColor whiteColor];
    self.signUpView.usernameField.textColor = [UIColor blackColor];
    self.signUpView.usernameField.placeholder = LOCALIZED(@"login_field_username");

    self.signUpView.passwordField.backgroundColor = [UIColor whiteColor];
    self.signUpView.passwordField.textColor = [UIColor blackColor];
    self.signUpView.passwordField.placeholder = LOCALIZED(@"login_field_password");

    self.signUpView.emailField.backgroundColor = [UIColor whiteColor];
    self.signUpView.emailField.textColor = [UIColor blackColor];
    self.signUpView.emailField.placeholder = LOCALIZED(@"login_field_email");


    //Signup Button
    [self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    [self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.signUpView.signUpButton setTitle:@"S'inscrire" forState:UIControlStateNormal];
    [self.signUpView.signUpButton setTitle:@"S'inscrire" forState:UIControlStateHighlighted];
    self.signUpView.signUpButton.backgroundColor = [UIColor colorWithRed:0.152941F green:0.682353F blue:0.376471F alpha:1.0F];
    self.signUpView.signUpButton.layer.cornerRadius = 3;
    self.signUpView.signUpButton.titleLabel.shadowOffset = CGSizeZero;
    [self.signUpView.signUpButton setShowsTouchWhenHighlighted:YES];

    [self.signUpView.dismissButton setImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    [self.signUpView.dismissButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];

}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    UIBezierPath *maskUsername =[UIBezierPath bezierPathWithRoundedRect:self.signUpView.usernameField.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(3.0, 3.0)];
    
    CAShapeLayer *layerUsername = [[CAShapeLayer alloc] init];
    layerUsername.frame = self.signUpView.usernameField.bounds;
    layerUsername.path = maskUsername.CGPath;
    self.signUpView.usernameField.layer.mask = layerUsername;
    
    UIBezierPath *maskEmail =[UIBezierPath bezierPathWithRoundedRect:self.signUpView.passwordField.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(3.0, 3.0)];
    
    CAShapeLayer *layerEmail = [[CAShapeLayer alloc] init];
    layerEmail.frame = self.signUpView.passwordField.bounds;
    layerEmail.path = maskEmail.CGPath;
    self.signUpView.emailField.layer.mask = layerEmail;
}
@end
