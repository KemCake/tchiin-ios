//
//  HowToViewController.swift
//  Thumbify
//
//  Created by Remi Santos on 18/12/14.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var footer: UIButton!

    let titleSteps = ["Yay!", "1.", "2.", "3.", "Hooray!"];
    let descSteps = ["Add your awesome Thumbify keyboard in seconds \\o/!", "Go to your device Settings.\nEasy.", "Go to Keyboard.\nStill easy :)", "Add the new keyboard.\nAnd you're done! 👍", "Put your thumbs \n in the air!"];
    let imageSteps = ["bar1_blur","bar1_blur","bar1_blur","bar1_blur", "bar1_blur"]
    var animator = IFTTTAnimator()
    
    let cellInset = 60 as CGFloat
    var cellWidth:CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.clipsToBounds = true
    
        cellWidth = self.view.frame.size.width - cellInset*2
        
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    @IBAction func close(sender:UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.scrollViewDidScroll(self.collectionView)
    }
    @IBAction func muxuButtonClicked(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string:"http://muxumuxu.com")!)
    }
    
    //MARK: CollectionView
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(cellWidth, self.view.frame.size.height)
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, cellInset, 0, cellInset)
    }
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = titleSteps.count
        return titleSteps.count
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("StepCell", forIndexPath: indexPath) as StepCell
        cell.titleLabel.text = titleSteps[indexPath.row]
        cell.descLabel.text = descSteps[indexPath.row]
        cell.imageView.image = UIImage(named: imageSteps[indexPath.row])
        return cell
    }
    
    
    //MARK: Scroll
    func parallaxPositionForCell(cell:StepCell) -> CGFloat {
        let frame = cell.frame
        let point = cell.superview?.convertPoint(frame.origin ,toView:collectionView)
        
        let minX = CGRectGetMinX(collectionView.bounds) - frame.size.width
        let maxX = CGRectGetMaxX(collectionView.bounds)
        
        let minPos = -1.0 as CGFloat
        let maxPos = 1.0 as CGFloat
        
        return (maxPos - minPos) / (maxX - minX) * (point!.x - minX) + minPos;
    }
    
    func convertXToPage(x:CGFloat) -> Int {
        return Int(x / (cellWidth))
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let x = Int(scrollView.contentOffset.x)
        animator.animate(x)
        for cell in collectionView.visibleCells() as [StepCell] {
            let position = parallaxPositionForCell(cell)
            cell.parallaxPosition = position
        }
    }
    
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        var positionPage = pageControl.currentPage
        var targetPage = convertXToPage(targetContentOffset.memory.x)
        if targetPage > positionPage {
            positionPage = min(positionPage+1, titleSteps.count-1)
        } else if targetPage < positionPage {
            positionPage = max(positionPage-1, 0)
        }
        let offset = cellWidth * CGFloat(positionPage)
        targetContentOffset.memory.x = offset
        pageControl.currentPage = positionPage
    }

}
