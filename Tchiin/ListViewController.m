//
//  ListViewController.m
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "ListViewController.h"

@implementation ListViewController

static NSString *placeCellIdentifier = @"PlaceCell";

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.emptyDataSetSource = self;
    self.tableView.contentSize = CGSizeMake(self.view.frame.size.width, 100);
}
-(void)placesUpdated
{
    [self.tableView reloadData];
}


#pragma mark - UITableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.parent.isSearching)
        return self.parent.searchPlaces.count;
    else
        return self.parent.places.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:placeCellIdentifier forIndexPath:indexPath];

    Place *place;
    if (self.parent.isSearching) {
        place = self.parent.searchPlaces[indexPath.row];
    } else {
        place = self.parent.places[indexPath.row];
    }
    UILabel *nameLabel = (UILabel*)[cell.contentView viewWithTag:1];
    nameLabel.text = place.name;
    
    __block UILabel *addressLabel = (UILabel*)[cell.contentView viewWithTag:2];
    addressLabel.text = place.address;
    [place getCoordinatesWithBlock:^(CLLocation *location) {
        CLLocationDistance distance = [location distanceFromLocation:[RSLocationManager sharedInstance].location];
        
        addressLabel.text = [NSString stringWithFormat:@"%@ - %@",[self.parent.distanceFormatter stringFromDistance:distance], place.address];
    }];
    
    UIImageView *imageView = (UIImageView*)[cell.contentView viewWithTag:3];
    [imageView setImageWithURL:[NSURL URLWithString:place.imageUrl] placeholderImage:[UIImage imageNamed:@"default_photo"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"PlaceSegue" sender:nil];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"PlaceSegue"]) {
        PlaceViewController *destination = segue.destinationViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        if (self.parent.isSearching)
            destination.place = self.parent.searchPlaces[indexPath.row];
        else
            destination.place = self.parent.places[indexPath.row];
    }
}


#pragma mark - UITableView data set
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSString *text = LOCALIZED(@"no_places_found_title");
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    
    NSString *text = LOCALIZED(@"no_places_found_subtitle");
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:14.0],
                                 NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                 NSParagraphStyleAttributeName: paragraph};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}
@end
