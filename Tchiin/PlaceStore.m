//
//  PlaceStore.m
//  Tchiin
//
//  Created by Remi Santos on 19/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "PlaceStore.h"

@implementation PlaceStore

+ (id)sharedInstance {
    static PlaceStore *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        
    });
    return sharedInstance;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        yelpService = [[YelpAPIService alloc] init];
        yelpService.delegate = self;
    }
    return self;
}


#pragma mark - Get Multiple places
-(void)searchPlacesWithTerm:(NSString*)term withBlock:(void (^)(NSArray* places))block
{
    currentBlock = block;
    [yelpService searchNearByPlacesByTerm:[term urlencode]];
}

-(void)searchPlacesAroundLocation:(CLLocation*)location withTerm:(NSString*)term withBlock:(void (^)(NSArray* places))block
{
    currentBlock = block;
    [yelpService searchNearByPlacesByTerm:[term urlencode] atLatitude:location.coordinate.latitude andLongitude:location.coordinate.longitude];
}

-(void)searchPlacesAroundLocation:(CLLocation*)location withBlock:(void (^)(NSArray* places))block
{
    currentBlock = block;
    [yelpService searchNearByPlacesByTerm:@"" atLatitude:location.coordinate.latitude andLongitude:location.coordinate.longitude];
}
-(void)searchPlacesAroundLocation:(CLLocation*)location withRadiusFilter:(NSInteger)radiusFilter withBlock:(void (^)(NSArray* places))block
{
    currentBlock = block;
    [yelpService searchNearByPlacesAtLatitude:location.coordinate.latitude andLongitude:location.coordinate.longitude withRadiusFilter:radiusFilter];
}
-(void)getAllPlaceBetweenNorthEast:(CLLocation*)ne andSouthWest:(CLLocation*)sw withBlock:(void (^)(NSArray* places))block
{
    
    currentBlock = block;
    [yelpService searchNearByPlacesByTerm:@"" betweenNorthEast:ne.coordinate andSouthWest:sw.coordinate];
}

#pragma mark - Get Place
-(void)getPlaceById:(NSString*)yelpId withBlock:(void (^)(NSArray* result))block
{
    currentBlock = block;
    [yelpService getPlaceById:yelpId];
}

#pragma mark - Yelp delegate
-(void)loadResultWithDataArray:(NSArray *)resultArray
{
    if (currentBlock) {
        currentBlock(resultArray);
    }
}
@end
