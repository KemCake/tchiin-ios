//
//  ListViewController.h
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapListViewController.h"
#import "PlaceViewController.h"
#import "UIScrollView+EmptyDataSet.h"
@class MapListViewController;
@interface ListViewController : UITableViewController<DZNEmptyDataSetSource>
{
}
@property (nonatomic, strong) MapListViewController* parent;

-(void)placesUpdated;

@end
