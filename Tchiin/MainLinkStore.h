//
//  MainLinkStore.h
//  Tchiin
//
//  Created by Remi Santos on 20/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainLinkStore : NSObject
+(void)getAllMainLinksWithBlock:(void (^)(NSMutableArray* mainLinks))block;

@end
