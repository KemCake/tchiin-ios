//
//  YelpAPIService.h
//  YelpNearby
//
//  Created by Behera, Subhransu on 8/14/13.
//  Copyright (c) 2013 Behera, Subhransu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OAuthConsumer.h"

@protocol YelpAPIServiceDelegate <NSObject>
-(void)loadResultWithDataArray:(NSArray *)resultArray;
@end

@interface YelpAPIService : NSObject <NSURLConnectionDataDelegate>

@property(nonatomic, strong) NSMutableData *urlRespondData;
@property(nonatomic, strong) NSString *responseString;
@property(nonatomic, strong) NSMutableArray *resultArray;

@property (weak, nonatomic) id <YelpAPIServiceDelegate> delegate;

-(void)searchNearByPlacesByTerm:(NSString *)term;

-(void)searchNearByPlacesByTerm:(NSString *)categoryFilter atLatitude:(CLLocationDegrees)latitude
                          andLongitude:(CLLocationDegrees)longitude;

-(void)searchNearByPlacesByTerm:(NSString *)categoryFilter betweenNorthEast:(CLLocationCoordinate2D)ne
                     andSouthWest:(CLLocationCoordinate2D)sw;

-(void)searchNearByPlacesAtLatitude:(CLLocationDegrees)latitude
                       andLongitude:(CLLocationDegrees)longitude
                   withRadiusFilter:(NSInteger)radiusFilter;

-(void)getPlaceById:(NSString*)yelpId;

@end
