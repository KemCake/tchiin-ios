//
//  UIColor+Custom.m
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "UIColor+Custom.h"

@implementation UIColor (Custom)

+(UIColor*)tchiinBrown{
    return [UIColor colorWithRed:0.705882F green:0.313725F blue:0.156863F alpha:1.0F];
}
+(UIColor*)tchiinOrange{
    return [UIColor colorWithRed:0.937255F green:0.521569F blue:0.090196F alpha:1.0F];
}

+(UIColor*)tchiinOrangeLight{
    return [UIColor colorWithRed:0.988235F green:0.752941F blue:0.078431F alpha:1.0F];
}

+(UIColor*)tchiinComplementaryLightBlue{
    return [UIColor colorWithRed:0.000000F green:0.607843F blue:0.980392F alpha:1.0F];
}

+(UIColor*)tchiinComplementaryDarkBlue{
    return [UIColor colorWithRed:0.000000F green:0.423529F blue:0.678431F alpha:1.0F];
}
+(UIColor*)tchiinBlueColor{
    return [UIColor colorWithRed:0.000000F green:0.211765F blue:0.349020F alpha:1.0F];//[UIColor colorWithRed:0.043137F green:0.176471F blue:0.243137F alpha:1.0F];
}

+(UIColor*)facebookColor{
    return [UIColor colorWithRed:0.231373F green:0.349020F blue:0.596078F alpha:1.0F];
}

+(UIColor*)yelpColor{
    return [UIColor colorWithRed:0.788235F green:0.039216F blue:0.066667F alpha:1.0F];
}

@end
