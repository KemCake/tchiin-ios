//
//  TNLoginViewController.h
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <Parse/Parse.h>
#import "TNSignUpViewController.h"
#import "EAIntroView.h"
#import "KLCPopup.h"

@interface TNLoginViewController : PFLogInViewController
{
    EAIntroView *intro;
    UIImageView *introImageTitle;
    UIImageView *introLogoTitle;
    

    UIView *introIconsTitle;
    UIImageView *iconA;
    UIImageView *iconB;
    UIImageView *iconC;
    
    UIView *introLastTitle;
    UIImageView *smileyA;
    UIImageView *smileyB;
    UIImageView *smileyC;
    UIImageView *smileyD;
    UILabel *subtitleView;
    UIButton *connectBlockButton;
}

@property (nonatomic, strong) KLCPopup* popup;
@end
