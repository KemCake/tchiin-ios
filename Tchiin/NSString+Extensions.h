//
//  NSString+Extensions.h
//  Tchiin
//
//  Created by Remi Santos on 08/11/14.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extensions)
- (NSString *)urlencode;
@end
