//
//  AdviceStore.m
//  Tchiin
//
//  Created by Remi Santos on 20/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "AdviceStore.h"

@implementation AdviceStore

+(void)getAllAdvicesWithBlock:(void (^)(NSMutableArray* advices))block
{
    PFQuery *query = [PFQuery queryWithClassName:@"Advice"];
    [query orderByAscending:@"order"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSMutableArray *result = [NSMutableArray arrayWithArray:objects];
        block(result);
    }];
}

@end
