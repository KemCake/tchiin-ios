//
//  PopupViewController.swift
//  Tchiin
//
//  Created by Remi Santos on 20/10/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

import UIKit

@objc protocol PopupViewControllerDelegate:NSObjectProtocol {
    optional func popupDidDismissWithIndex(index:NSInteger)
}

class PopupViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(animated: Bool) {
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func dismissPopup(sender:AnyObject) {
        let parent = self.presentingPopinViewController()
        parent.dismissCurrentPopinControllerAnimated(true, completion: { () -> Void in
            (parent as PopupViewControllerDelegate).popupDidDismissWithIndex!(self.view.tag)
        })
    }

}
