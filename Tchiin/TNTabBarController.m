//
//  TNTabBarController.m
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
#import "TNTabBarController.h"

@implementation TNTabBarController

-(void)viewDidAppear:(BOOL)animated
{
    [self showLogin];
    if (![PFUser currentUser]) {
        [PFAnonymousUtils logInWithBlock:^(PFUser *user, NSError *error) {
            [self registerForNotifications];
        }];
    }

}
-(void)showLogin
{
    logInViewController = [[TNLoginViewController alloc] init];
    [logInViewController setDelegate:self]; // Set ourselves as the delegate
    [logInViewController setFacebookPermissions:@[@"friends_about_me"]];
    [logInViewController setFields: PFLogInFieldsFacebook];
    
    
    // Create the sign up view controller
    signUpViewController = [[TNSignUpViewController alloc] init];
    [signUpViewController setDelegate:self]; // Set ourselves as the delegate
    
    // Assign our sign up controller to be displayed from the login controller
    [logInViewController setSignUpController:signUpViewController];
    [self presentViewController:logInViewController animated:YES completion:nil];
}
-(void)logout
{
    [PFUser logOut];
    [[PFInstallation currentInstallation] removeObjectForKey:@"user"];
    [[PFInstallation currentInstallation] saveInBackground];
//    [self showLogin];
}
-(void)closeLogin
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - Login / Signup delegate
- (void)registerForNotifications {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:
         [UIUserNotificationSettings settingsForTypes:
          (UIUserNotificationTypeSound |
           UIUserNotificationTypeAlert |
           UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert|
                                                                               UIRemoteNotificationTypeBadge|
                                                                               UIRemoteNotificationTypeSound)];
    }

}

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self registerForNotifications];
    
    [self closeLogin];
}
-(void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error
{
    NSLog(@"error: %@",error);
}
-(void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
    [logInViewController.popup dismiss:YES];
    [self closeLogin];
}
@end
