//
//  PlaceViewController.h
//  Tchiin
//
//  Created by Remi Santos on 26/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "UIKit+AFNetworking.h"
#import "MKMapView+Utils.h"
#import "PlaceStore.h"
#import "UIImage+ImageEffects.h"
#import "Tchiin-Swift.h"
#import "MapListViewController.h"
#import "UIScrollView+EmptyDataSet.h"
@interface PlaceViewController : UIViewController<MKMapViewDelegate, UITableViewDataSource,UITableViewDelegate, DZNEmptyDataSetSource>
{
    BOOL failedToLoad;
}
@property (strong, nonatomic) Place *place;
@property (strong, nonatomic) NSDictionary *launchOptions;
@property (strong, nonatomic) IBOutlet UITableView *tableView;


@end
