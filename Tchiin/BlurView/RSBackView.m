//
//  AMBlurView.m
//  blur
//
//  Created by Cesar Pinto Castillo on 7/1/13.
//  Copyright (c) 2013 Arctic Minds Inc. All rights reserved.
//

#import "RSBackView.h"

@interface RSBackView ()

@end

@implementation RSBackView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
-(void)awakeFromNib
{
    if (!_blurEffectStyle)
        self.blurEffectStyle = UIBlurEffectStyleExtraLight;
    [self setup];
}
-(void)setup
{
    UIVisualEffect *effect = [UIBlurEffect effectWithStyle:self.blurEffectStyle];
    self.backgroundView = [[UIVisualEffectView alloc] initWithEffect:effect];
    self.backgroundView.contentView.backgroundColor = self.backgroundColor;
    self.backgroundColor = [UIColor clearColor];
    self.backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
//    backgroundView.alpha = 0.5;
    [self insertSubview:self.backgroundView atIndex:0];
    NSDictionary *views = @{@"backgroundView" : self.backgroundView};
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[backgroundView]|" options:0 metrics:nil views:views]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[backgroundView]|" options:0 metrics:nil views:views]];
    self.backgroundColor = [UIColor clearColor];
}
//- (void)setup {
    // If we don't clip to bounds the toolbar draws a thin shadow on top
//    [self setClipsToBounds:YES];
//    self.backgroundColor = [UIColor clearColor];
//    if (![self toolbar]) {
//        [self setToolbar:[[UIToolbar alloc] initWithFrame:[self bounds]]];
//        [self.toolbar setTranslatesAutoresizingMaskIntoConstraints:NO];
//        [self insertSubview:[self toolbar] atIndex:0];
//        
//        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_toolbar]|"
//                                                                     options:0
//                                                                     metrics:0
//                                                                       views:NSDictionaryOfVariableBindings(_toolbar)]];
//        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_toolbar]|"
//                                                                     options:0
//                                                                     metrics:0
//                                                                       views:NSDictionaryOfVariableBindings(_toolbar)]];
//    }
//}

- (void) setBlurTintColor:(UIColor *)blurTintColor {
    [self.toolbar setBarTintColor:blurTintColor];
}

@end
