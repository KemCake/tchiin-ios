//
//  HomeMainLinksCollectionViewController.m
//  Tchiin
//
//  Created by Remi Santos on 20/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "HomeMainLinksCollectionViewController.h"
#import "FontAwesomeKit.h"
#import "RSBackView.h"

@implementation HomeMainLinksCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bar1_blur"]];
    self.collectionView.backgroundColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)mainLinksUpdated
{
    [self.collectionView reloadData];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.parent.mainLinks.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"MainLinkCell";

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    PFObject *mainLink = self.parent.mainLinks[indexPath.row];
    
    UILabel *nameLabel = (UILabel*)[cell.contentView viewWithTag:1];
    nameLabel.text = mainLink[@"title"];
    
    UILabel *subLabel = (UILabel*)[cell.contentView viewWithTag:2];
    subLabel.text = mainLink[@"subtitle"];
    
    RSBackView *backView = (RSBackView*)[cell.contentView viewWithTag:3];
    backView.layer.cornerRadius = backView.frame.size.width / 2;
    backView.clipsToBounds = TRUE;
    
    UIImageView *iconView = (UIImageView*)[cell.contentView viewWithTag:4];
    iconView.alpha = 0.8;
    if([mainLink[@"icon"] isEqualToString:@"glass"])
        iconView.image = [UIImage imageNamed:@"cocktail_white"];
    else if([mainLink[@"icon"] isEqualToString:@"wine"])
        iconView.image = [UIImage imageNamed:@"wine_white"];
    else
        iconView.image = [UIImage imageNamed:@"beer_white"];
    
    return cell;
}



@end
