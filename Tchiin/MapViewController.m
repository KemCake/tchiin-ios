//
//  MapViewController.m
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "MapViewController.h"

@implementation MapViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    [RSLocationManager sharedInstance].delegate = self;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
        [[RSLocationManager sharedInstance] requestWhenInUseAuthorization];
    }
    [[RSLocationManager sharedInstance] startUpdatingLocation];
    self.mapView.showsUserLocation = true;
    self.mapView.delegate = self;
    
    MKUserTrackingBarButtonItem *trackingButton = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    NSMutableArray *items = [NSMutableArray arrayWithArray:self.toolbar.items];
    [items insertObject:trackingButton atIndex:0];
    self.toolbar.items = items;
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.color = self.toolbar.tintColor;
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicator];
    NSMutableArray *toolBarItems = [self.toolbar.items mutableCopy];
    [toolBarItems insertObject:barButton atIndex:2];
    self.toolbar.items = toolBarItems;
}
-(void)viewDidAppear:(BOOL)animated
{
    [self setShowToolbar:true];
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        _toolbar.transform = CGAffineTransformMakeTranslation(0, 44);
    }
}
-(void)setShowToolbar:(BOOL)showToolbar
{
    _showToolbar = showToolbar;
    [self.toolbar setHidden:!showToolbar];
}
- (IBAction)reloadRegion:(id)sender {
    bounds bounds = [self.mapView fetchBounds];
    CLLocation *ne = [[CLLocation alloc] initWithLatitude:bounds.maxLatitude longitude:bounds.maxLongitude];
    CLLocation *sw = [[CLLocation alloc] initWithLatitude:bounds.minLatitude longitude:bounds.minLongitude];

    [self.parent loadPlacesForNorthEast:ne andSouthWest:sw];
}

-(void)placesUpdated
{
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    for (Place *place in self.parent.isSearching?self.parent.searchPlaces:self.parent.places) {
        [place getCoordinatesWithBlock:^(CLLocation *location) {
            if (location.coordinate.latitude != 0 || location.coordinate.longitude != 0) {
                TNPointAnnotation *point = [[TNPointAnnotation alloc] init];
                point.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
                point.title = place.name;
                point.subtitle = place.address;
                point.place = place;
                CLLocationDistance distance = [location distanceFromLocation:[RSLocationManager sharedInstance].location];
                point.subtitle = [self.parent.distanceFormatter stringFromDistance:distance];
                [self.mapView addAnnotation:point];
                [self zoomToFitAnnotations];
            }
        }];
    }
}

-(void)zoomToFitAnnotations {
    [self.mapView zoomToFitWithAnnotations:self.mapView.annotationsWithoutUserLocation animated:true];
}
-(void)loadPlacesHere
{
    [self reloadRegion:nil];
}
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
    else
    {
        static NSString * const identifier = @"pin_orange";
        
        MKAnnotationView* annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView)
        {
            annotationView.annotation = annotation;
        }
        else
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        }
        annotationView.image = [UIImage imageNamed:identifier];
        annotationView.canShowCallout = YES;
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeSystem];
        rightButton.frame = CGRectMake(0, 0, 25, 25);
        [rightButton setImage:[UIImage imageNamed:@"right_arrow"] forState:UIControlStateNormal];
        [rightButton setTitle:@"Voir" forState:UIControlStateNormal];

        annotationView.rightCalloutAccessoryView = rightButton;
        return annotationView;
    }
    
    return nil;
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    TNPointAnnotation *annotation = (TNPointAnnotation*)view.annotation;
    
    PlaceViewController *destination = [self.storyboard instantiateViewControllerWithIdentifier:@"PlaceVC"];
    destination.place = annotation.place;
    
    [self.navigationController pushViewController:destination animated:true];
}

#pragma mark - MapView delegate
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
    if(self.loadOnLocation)
    {
        self.loadOnLocation = false;
        MKCoordinateSpan span = MKCoordinateSpanMake(0.005, 0.005);
        CLLocationCoordinate2D location = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
        MKCoordinateRegion region = MKCoordinateRegionMake(location, span);
        [self.mapView setRegion:region animated:true];
        [self.parent loadPlacesForLocation:userLocation.location];        
    }
    
    PFGeoPoint *geopoint = [PFGeoPoint geoPointWithLocation:userLocation.location];
    PFUser *currentUser = [PFUser currentUser];
    [currentUser setObject:geopoint forKey:@"last_location"];
    [currentUser saveEventually];
}
@end
