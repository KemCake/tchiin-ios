//
//  SubscriptionStore.swift
//  Tchiin
//
//  Created by Remi Santos on 13/10/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

import UIKit

class SubscriptionStore {
 
    class func getAllSubscriptionsForCurrentUserWithBlock(resultBlock:([PFObject])->Void) {
        var query = PFQuery(className: "Subscription")
        query.includeKey("users")
        query.whereKey("users", containsAllObjectsInArray: [PFUser.currentUser()])
        query.findObjectsInBackgroundWithBlock { (objects, error) -> Void in
            var result = objects
            if(result == nil) {
                result = []
            }
            resultBlock(result as [PFObject])

        }
    }
    
    class func subscribeWithCode(code:String, withBlock block:(succeed:Bool)->Void) {
        
        PFCloud.callFunctionInBackground("subscribe", withParameters: ["code":code]) { (response, error) -> Void in
            block(succeed: error == nil)
        }
        
    }
    
    class func unsubscribeWithId(subscriptionId:String, withBlock block:(succeed:Bool)->Void) {
        
        PFCloud.callFunctionInBackground("unsubscribe", withParameters: ["subscription_id":subscriptionId]) { (response, error) -> Void in
            block(succeed: error == nil)
        }
        
    }
}
