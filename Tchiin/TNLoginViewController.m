//
//  TNLoginViewController.m
//  Tchiin
//
//  Created by Remi Santos on 18/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import "TNLoginViewController.h"
@implementation TNLoginViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bar1"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.logInView setBackgroundColor:[UIColor colorWithPatternImage:image]];
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_white_orange"]];
    logo.frame = CGRectMake(0, 0, 200, 80);
    logo.contentMode = UIViewContentModeScaleAspectFit;
    [self.logInView setLogo:logo];
    self.logInView.dismissButton.hidden = YES;
    
    self.logInView.usernameField.backgroundColor = [UIColor whiteColor];
    self.logInView.usernameField.placeholder = LOCALIZED(@"login_field_username");
    self.logInView.usernameField.textColor = [UIColor blackColor];
    self.logInView.passwordField.backgroundColor = [UIColor whiteColor];
    self.logInView.passwordField.textColor = [UIColor blackColor];
    self.logInView.passwordField.placeholder = LOCALIZED(@"login_field_password");

    
    //Facebook Button
    [self.logInView.facebookButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    [self.logInView.facebookButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.logInView.facebookButton setTitle:@" Facebook Connect" forState:UIControlStateNormal];
    [self.logInView.facebookButton setTitle:@" Facebook Connect" forState:UIControlStateHighlighted];
    self.logInView.facebookButton.backgroundColor = [UIColor facebookColor];
    self.logInView.facebookButton.layer.cornerRadius = 3;
    
    //Login Button
    [self.logInView.logInButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    [self.logInView.logInButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.logInView.logInButton setTitle:LOCALIZED(@"login") forState:UIControlStateNormal];
    [self.logInView.logInButton setTitle:LOCALIZED(@"login") forState:UIControlStateHighlighted];
    self.logInView.logInButton.backgroundColor = [UIColor tchiinOrange];
    self.logInView.logInButton.layer.cornerRadius = 3;
    self.logInView.logInButton.titleLabel.shadowOffset = CGSizeZero;
    [self.logInView.logInButton setShowsTouchWhenHighlighted:YES];
    //Forgot Button
    [self.logInView.passwordForgottenButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    [self.logInView.passwordForgottenButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setImage:[UIImage imageNamed:@"help"] forState:UIControlStateNormal];
    [self.logInView.passwordForgottenButton setImage:[UIImage imageNamed:@"help"] forState:UIControlStateHighlighted];
    self.logInView.passwordForgottenButton.backgroundColor = [UIColor tchiinOrange];
    self.logInView.passwordForgottenButton.layer.cornerRadius = 3;
    self.logInView.passwordForgottenButton.imageEdgeInsets = UIEdgeInsetsMake(13, -6, 13, -4);
    //Signup Button
    [self.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    [self.logInView.signUpButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self.logInView.signUpButton setTitle:LOCALIZED(@"signup") forState:UIControlStateNormal];
    [self.logInView.signUpButton setTitle:LOCALIZED(@"signup") forState:UIControlStateHighlighted];
    [self.logInView.signUpButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [self.logInView.signUpButton addTarget:self action:@selector(showSignUp) forControlEvents:UIControlEventTouchUpInside];
    self.logInView.signUpButton.backgroundColor = [UIColor colorWithRed:0.152941F green:0.682353F blue:0.376471F alpha:1.0F];
    self.logInView.signUpButton.layer.cornerRadius = 3;
    self.logInView.signUpButton.titleLabel.shadowOffset = CGSizeZero;
    [self.logInView.signUpButton setShowsTouchWhenHighlighted:YES];
    
    self.logInView.externalLogInLabel.textColor = [UIColor whiteColor];
    self.logInView.externalLogInLabel.text = LOCALIZED(@"login_also_with");
    self.logInView.signUpLabel.textColor = [UIColor whiteColor];
    self.logInView.signUpLabel.text = LOCALIZED(@"login_no_account_yet");

    [self setupIntro];
    [self showIntro];
    
}
-(void)showSignUp
{
    UIView *contentView = self.signUpController.view;
    contentView.frame = CGRectMake(25, 80, 300, 300);
    self.popup = [KLCPopup popupWithContentView:contentView];
    self.popup.showType = KLCPopupShowTypeBounceInFromTop;
    self.popup.dismissType = KLCPopupDismissTypeBounceOutToBottom;
    self.popup.dimmedMaskAlpha = 0.5;
    [self.popup show];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    UIBezierPath *maskUsername =[UIBezierPath bezierPathWithRoundedRect:self.logInView.usernameField.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(3.0, 3.0)];
    
    CAShapeLayer *layerUsername = [[CAShapeLayer alloc] init];
    layerUsername.frame = self.logInView.usernameField.bounds;
    layerUsername.path = maskUsername.CGPath;
    self.logInView.usernameField.layer.mask = layerUsername;
    
    UIBezierPath *maskPassword =[UIBezierPath bezierPathWithRoundedRect:self.logInView.passwordField.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(3.0, 3.0)];
    
    CAShapeLayer *layerPassword = [[CAShapeLayer alloc] init];
    layerPassword.frame = self.logInView.passwordField.bounds;
    layerPassword.path = maskPassword.CGPath;
    self.logInView.passwordField.layer.mask = layerPassword;
    
    self.logInView.passwordForgottenButton.center = CGPointMake(self.logInView.passwordForgottenButton.center.x+3, self.logInView.passwordForgottenButton.center.y);
    [self.logInView sendSubviewToBack:self.logInView.passwordForgottenButton];
    
    self.logInView.logInButton.center = CGPointMake(self.logInView.logInButton.center.x, self.logInView.logInButton.center.y-10);
    [self.logInView sendSubviewToBack:self.logInView.logInButton];
    
    self.logInView.logo.center = CGPointMake(self.logInView.logo.center.x, self.logInView.logo.center.y - 100);
}





//////////////////////////////////////////////////////////////////////////////
#pragma mark - INTRO

- (void)setupIntro {
    EAIntroPage *page1 = [EAIntroPage page];
    page1.title = LOCALIZED(@"intro_title_1");
    page1.titlePositionY = 250;
    page1.titleFont = [UIFont boldSystemFontOfSize:22];
    page1.onPageDidAppear = ^{
        
        if (intro.titleView != introLogoTitle) {
            
            POPSpringAnimation *anim = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionX];
            anim.toValue = @(650);
            anim.springBounciness = 10;
            anim.springSpeed = 15;
            [anim setCompletionBlock:^(POPAnimation *a, BOOL b) {
                intro.titleViewY = 100;
                intro.titleView = introLogoTitle;
                POPSpringAnimation *anim = [POPSpringAnimation animationWithPropertyNamed: kPOPLayerPositionX];
                anim.fromValue = @(-100);
                anim.toValue = IS_IPHONE_6?@(185):@(160);
                anim.springBounciness = 15;
                anim.springSpeed = 15;

                [intro.titleView.layer pop_addAnimation:anim forKey:@"x"];
            }];
            [intro.titleView.layer pop_addAnimation:anim forKey:@"x"];
        }

    };
    EAIntroPage *page2 = [EAIntroPage page];
    page2.title = LOCALIZED(@"intro_title_2");
    page2.titlePositionY = 250;
    page2.titleFont = page1.titleFont;
    page2.onPageDidAppear = ^{
        POPSpringAnimation *anim = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionX];
        int toValue = -300;
        if(intro.titleView == introLastTitle)
            toValue = 650;
        anim.toValue = @(toValue);
        anim.springBounciness = 10;
        anim.springSpeed = 15;
        [anim setCompletionBlock:^(POPAnimation *a, BOOL b) {
            intro.titleView = introIconsTitle;
            intro.titleViewY = 0;
            POPSpringAnimation *anim = [POPSpringAnimation animationWithPropertyNamed: kPOPLayerPositionY];
            anim.fromValue = @(-30);
            anim.toValue = @(200);
            anim.springBounciness = 10;
            anim.springSpeed = 11;
            [iconA.layer pop_addAnimation:anim forKey:@"y"];
            POPSpringAnimation *animB = [POPSpringAnimation animationWithPropertyNamed: kPOPLayerPositionY];
            animB.fromValue = @(-30);
            animB.toValue = @(200);
            animB.springBounciness = 10;
            animB.springSpeed = 8;
            [iconB.layer pop_addAnimation:animB forKey:@"y"];
            POPSpringAnimation *animC = [POPSpringAnimation animationWithPropertyNamed: kPOPLayerPositionY];
            animC.fromValue = @(-30);
            animC.toValue = @(200);
            animC.springBounciness = 10;
            animC.springSpeed = 5;
            [iconC.layer pop_addAnimation:animC forKey:@"y"];
        }];
        [intro.titleView.layer pop_addAnimation:anim forKey:@"y"];
        
    };
    EAIntroPage *page3 = [EAIntroPage page];
    page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"glass_white"]];
    page3.title = LOCALIZED(@"intro_title_3");
    page3.titlePositionY = 250;
    page3.titleFont = page1.titleFont;
    page3.onPageDidAppear = ^{
        POPSpringAnimation *anim = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPositionX];
        anim.toValue = @(-300);
        anim.springBounciness = 10;
        anim.springSpeed = 15;
        [anim setCompletionBlock:^(POPAnimation *a, BOOL b) {
            intro.titleView = introLastTitle;
            intro.titleViewY = 0;
            POPSpringAnimation *anim = [POPSpringAnimation animationWithPropertyNamed: kPOPLayerPosition];
            anim.fromValue = [NSValue valueWithCGPoint:CGPointMake(100, -50)];
            anim.toValue = [NSValue valueWithCGPoint:CGPointMake(IS_IPHONE_6?120:100, 200)];
            anim.springBounciness = 10;
            anim.springSpeed = 11;
            [smileyA.layer pop_addAnimation:anim forKey:@"y"];
            POPSpringAnimation *animB = [POPSpringAnimation animationWithPropertyNamed: kPOPLayerPosition];
            animB.fromValue = [NSValue valueWithCGPoint:CGPointMake(180, -50)];
            animB.toValue = [NSValue valueWithCGPoint:CGPointMake(IS_IPHONE_6?200:180, 170)];
            animB.springBounciness = 10;
            animB.springSpeed = 8;
            [smileyB.layer pop_addAnimation:animB forKey:@"y"];
            POPSpringAnimation *animC = [POPSpringAnimation animationWithPropertyNamed: kPOPLayerPosition];
            animC.fromValue = [NSValue valueWithCGPoint:CGPointMake(210, -50)];
            animC.toValue = [NSValue valueWithCGPoint:CGPointMake(IS_IPHONE_6?230:210, 230)];
            animC.springBounciness = 10;
            animC.springSpeed = 5;
            [smileyC.layer pop_addAnimation:animC forKey:@"y"];
            POPSpringAnimation *animD = [POPSpringAnimation animationWithPropertyNamed: kPOPLayerPosition];
            animD.fromValue = [NSValue valueWithCGPoint:CGPointMake(260, -50)];
            animD.toValue = [NSValue valueWithCGPoint:CGPointMake(IS_IPHONE_6?280:260, 190)];
            animD.springBounciness = 10;
            animD.springSpeed = 10;
            [smileyD.layer pop_addAnimation:animD forKey:@"y"];
        }];
        [intro.titleView.layer pop_addAnimation:anim forKey:@"y"];
    };
    
    
    intro = [[EAIntroView alloc] initWithFrame:self.logInView.bounds andPages:@[page1,page2,page3]];
    //[intro setDelegate:self];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bar1"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    intro.backgroundColor = [UIColor colorWithPatternImage:image];
    
    introImageTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"beer_white"]];
    introImageTitle.frame = CGRectMake(0, 0, 100, 100);
    introImageTitle.contentMode = UIViewContentModeScaleAspectFit;
    
    introLogoTitle = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_white_orange"]];
    introLogoTitle.frame = CGRectMake(0, 0, self.view.frame.size.width - 50, 200);
    introLogoTitle.contentMode = UIViewContentModeScaleAspectFit;

    intro.titleView = introLogoTitle;
    intro.titleViewY = 100;
    
    
    
    
    // ICONS (page 2)
    introIconsTitle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    introIconsTitle.clipsToBounds = NO;

    iconA = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wine_round"]];
    iconA.frame = CGRectMake(IS_IPHONE_6?58:28, 0, 70, 70);
    iconA.backgroundColor = [UIColor whiteColor];
    iconA.layer.cornerRadius = 35;
    [introIconsTitle addSubview:iconA];
    iconB = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"beer_round"]];
    iconB.frame = CGRectMake(IS_IPHONE_6?155:125, 0, 70, 70);
    iconB.backgroundColor = [UIColor whiteColor];
    iconB.layer.cornerRadius = 35;
    [introIconsTitle addSubview:iconB];
    iconC = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cocktail_round"]];
    iconC.frame = CGRectMake(IS_IPHONE_6?252:222, 0, 70, 70);
    iconC.backgroundColor = [UIColor whiteColor];
    iconC.layer.cornerRadius = 35;
    [introIconsTitle addSubview:iconC];
    
    
    //SMILEY (page3)

    introLastTitle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
    smileyA = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"smiley_A"]];
    smileyA.frame = CGRectMake(0, 0, 80, 80);
    smileyA.backgroundColor = [UIColor whiteColor];
    smileyA.layer.cornerRadius = 40;
    [introLastTitle addSubview:smileyA];
    smileyB = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"smiley_B"]];
    smileyB.frame = CGRectMake(0, 0, 40, 40);
    smileyB.backgroundColor = [UIColor whiteColor];
    smileyB.layer.cornerRadius = 20;
    [introLastTitle addSubview:smileyB];
    smileyC = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"smiley_C"]];
    smileyC.frame = CGRectMake(0, 0, 40, 40);
    smileyC.backgroundColor = [UIColor whiteColor];
    smileyC.layer.cornerRadius = 20;
    [introLastTitle addSubview:smileyC];
    smileyD = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"smiley_B"]];
    smileyD.frame = CGRectMake(0, 0, 40, 40);
    smileyD.backgroundColor = [UIColor whiteColor];
    smileyD.layer.cornerRadius = 20;
    [introLastTitle addSubview:smileyD];
    
    intro.pageControl = nil;
    [intro.skipButton setTitle:LOCALIZED(@"intro_skip") forState:UIControlStateNormal];
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
-(void)showIntro {
    [intro showInView:self.view animateDuration:1];
}
@end
