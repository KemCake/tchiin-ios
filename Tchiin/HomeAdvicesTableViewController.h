//
//  HomeAdvicesTableViewController.h
//  Tchiin
//
//  Created by Remi Santos on 20/07/2014.
//  Copyright (c) 2014 Remi Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "PlaceViewController.h"
#import "MapListViewController.h"
@class HomeViewController;
@interface HomeAdvicesTableViewController : UITableViewController

@property (nonatomic, strong) HomeViewController* parent;
@property (nonatomic, strong) NSDictionary* adviceParams;

-(void)advicesUpdated;
@end
